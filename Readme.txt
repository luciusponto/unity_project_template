- Create new version control repository:
	- lower case and underscores name
	- private if needed
- Add all contents of this folder **except** .git (folder) and readme.txt to new project folder.
- Create new Unity project on some random folder.
- Edit -> Project Settings -> Editor :
	Version Control: Visible Meta Files
	Asset Serialization: Force Text
- Close project and move all contents to folder under version control.
- Commit
- Delete Unity project on some random folder.

Also consider:
- Adding following Asset Store assets:
        TidyUp
        TextMesh Pro
        LeanTouch if deplyoing to mobile
- Creating project folder structure, either with TidyUp or manually.
- Move imported assets to _ImportedAssets folder. Exceptions that don't play nice:
